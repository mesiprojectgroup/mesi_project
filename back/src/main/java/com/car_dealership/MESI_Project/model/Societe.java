package com.car_dealership.MESI_Project.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Set;

@Entity
public class Societe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String nom;

    @NotNull
    private Double capital;

    @Pattern(regexp="[0-9]{3} [0-9]{3} [0-9]{3} [0-9]{5}")
    private String siret;

    @OneToMany(mappedBy = "societe", cascade = CascadeType.ALL)
    private Set<Concession> concessionSet;

    /*  Constructeur  */
    public Societe() {
    }

    /*  Getter et Setter  */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getCapital() {
        return capital;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public Set<Concession> getConcessionSet() {
        return concessionSet;
    }

    public void setConcessionSet(Set<Concession> concessionSet) {
        this.concessionSet = concessionSet;
    }

    /*  ToString  */

    @Override
    public String toString() {
        return "Societe{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", capital=" + capital +
                ", siret='" + siret + '\'' +
                ", concessionSet=" + concessionSet +
                '}';
    }
}
