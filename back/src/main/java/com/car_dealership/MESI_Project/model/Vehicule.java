package com.car_dealership.MESI_Project.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Vehicule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private TypeVehicule typeVehicule;

    private Double prix;

    private String modele;

    private String marque;

    private String color;

    @ManyToOne
    private Concession concession;

    @OneToOne
    private Facture facture;

    /*  Constructeur  */
    public Vehicule() {
    }

    /*  Getter et Setter  */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeVehicule getTypeVehicule() {
        return typeVehicule;
    }

    public void setTypeVehicule(TypeVehicule typeVehicule) {
        this.typeVehicule = typeVehicule;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Concession getConcession() {
        return concession;
    }

    public void setConcession(Concession concession) {
        this.concession = concession;
    }

    public Facture getFacture() {
        return facture;
    }

    public void setFacture(Facture facture) {
        this.facture = facture;
    }

    /*  ToString  */

    @Override
    public String toString() {
        return "Vehicule{" +
                "id=" + id +
                ", typeVehicule=" + typeVehicule +
                ", prix=" + prix +
                ", modele='" + modele + '\'' +
                ", marque='" + marque + '\'' +
                ", color='" + color + '\'' +
                ", concession=" + concession +
                ", facture=" + facture +
                '}';
    }
}
