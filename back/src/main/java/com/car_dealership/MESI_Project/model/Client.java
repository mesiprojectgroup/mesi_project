package com.car_dealership.MESI_Project.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Client extends Utilisateur {

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    private Set<Permis> permisSet;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    private Set<Facture> factureSet;

    /*  Constructeur */
    public Client() {
    }

    /*  Getter et Setter  */
    public Set<Permis> getPermis() {
        return permisSet;
    }

    public void setPermis(Set<Permis> permisSet) {
        this.permisSet = permisSet;
    }

    public Set<Facture> getFactureSet() {
        return factureSet;
    }

    public void setFactureSet(Set<Facture> factureSet) {
        this.factureSet = factureSet;
    }

    /*  ToString  */

    @Override
    public String toString() {
        return "Client{" +
                "permisSet=" + permisSet +
                ", factureSet=" + factureSet +
                '}';
    }
}
