package com.car_dealership.MESI_Project.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Set;

@Entity
public class Concession extends Auditable<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String adresse;

    @NotEmpty
    @Pattern(regexp="[\\d]{5}")
    private String cp;

    @NotEmpty
    private String ville;

    @OneToMany(mappedBy = "concession", cascade = CascadeType.ALL)
    private Set<Vehicule> vehiculeSet;

    @OneToOne
    private Gerant gerant;

    @ManyToOne
    private Societe societe;

    /*  Constructeur  */
    public Concession() {
    }

    /*  Getter et Setter  */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Set<Vehicule> getVehiculeSet() {
        return vehiculeSet;
    }

    public void setVehiculeSet(Set<Vehicule> vehiculeSet) {
        this.vehiculeSet = vehiculeSet;
    }

    public Gerant getGerant() {
        return gerant;
    }

    public void setGerant(Gerant gerant) {
        this.gerant = gerant;
    }

    public Societe getSociete() {
        return societe;
    }

    public void setSociete(Societe societe) {
        this.societe = societe;
    }

    /*  ToString  */

    @Override
    public String toString() {
        return "Concession{" +
                "id=" + id +
                ", adresse='" + adresse + '\'' +
                ", cp='" + cp + '\'' +
                ", ville='" + ville + '\'' +
                ", vehiculeSet=" + vehiculeSet +
                ", gerant=" + gerant +
                ", societe=" + societe +
                '}';
    }
}
