package com.car_dealership.MESI_Project.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Gerant extends Utilisateur {

    @NotNull
    private Double salaire;

    @OneToOne
    private Concession concession;

    /*  Constructeur  */
    public Gerant() {
    }

    /*  Getter et Setter  */
    public Double getSalaire() {
        return salaire;
    }

    public void setSalaire(Double salaire) {
        this.salaire = salaire;
    }

    /*  ToString  */

    @Override
    public String toString() {
        return "Gerant{" +
                "salaire=" + salaire +
                ", concession=" + concession +
                '}';
    }
}
