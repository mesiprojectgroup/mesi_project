package com.car_dealership.MESI_Project.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
public class Vendeur extends Utilisateur {

    @NotNull
    private Double RemiseMax;

    private Double chiffreAffaire;

    @NotNull
    private Double salaire;

    @OneToMany(mappedBy = "vendeur", cascade = CascadeType.ALL)
    private Set<Facture> factureSet;

    /*  Constructeur  */
    public Vendeur() {
    }

    /*  Getter et Setter  */
    public Double getRemiseMax() {
        return RemiseMax;
    }

    public void setRemiseMax(Double remiseMax) {
        RemiseMax = remiseMax;
    }

    public Double getChiffreAffaire() {
        return chiffreAffaire;
    }

    public void setChiffreAffaire(Double chiffreAffaire) {
        this.chiffreAffaire = chiffreAffaire;
    }

    public Double getSalaire() {
        return salaire;
    }

    public void setSalaire(Double salaire) {
        this.salaire = salaire;
    }

    public Set<Facture> getFactureSet() {
        return factureSet;
    }

    public void setFactureSet(Set<Facture> factureSet) {
        this.factureSet = factureSet;
    }

    /*  ToString  */

    @Override
    public String toString() {
        return "Vendeur{" +
                "RemiseMax=" + RemiseMax +
                ", chiffreAffaire=" + chiffreAffaire +
                ", salaire=" + salaire +
                ", factureSet=" + factureSet +
                '}';
    }
}
