package com.car_dealership.MESI_Project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Permis {

    @Id
    private Long id;

    @NotNull
    private String type;

    @ManyToOne
    private Client client;

    /*  Constructor  */
    public Permis() {
    }

    /*  Getter et Setter*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    /*  ToString  */

    @Override
    public String toString() {
        return "Permis{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", client=" + client +
                '}';
    }
}
