package com.car_dealership.MESI_Project.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Entity
public class TypeVehicule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String type;

    @OneToMany(mappedBy = "typeVehicule", cascade = CascadeType.ALL)
    private Set<Vehicule> vehiculeSet;

    /*  Constructeur  */
    public TypeVehicule() {
    }

    /*  Getter et Setter  */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Vehicule> getVehiculeSet() {
        return vehiculeSet;
    }

    public void setVehiculeSet(Set<Vehicule> vehiculeSet) {
        this.vehiculeSet = vehiculeSet;
    }

    /*  ToString  */

    @Override
    public String toString() {
        return "TypeVehicule{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", vehiculeSet=" + vehiculeSet +
                '}';
    }
}
