package com.car_dealership.MESI_Project.model;

import javax.persistence.*;

@Entity
public class Facture extends Auditable<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double remise;

    @ManyToOne
    private Client client;

    @ManyToOne
    private Vendeur vendeur;

    @OneToOne
    private Vehicule vehicule;

    /*  Constructeur  */

    public Facture() {
    }

    /*  Getter et Setter  */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getRemise() {
        return remise;
    }

    public void setRemise(Double remise) {
        this.remise = remise;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Vendeur getVendeur() {
        return vendeur;
    }

    public void setVendeur(Vendeur vendeur) {
        this.vendeur = vendeur;
    }

    public Vehicule getVehicule() {
        return vehicule;
    }

    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }

    /*  ToString  */

    @Override
    public String toString() {
        return "Facture{" +
                "id=" + id +
                ", remise=" + remise +
                ", client=" + client +
                ", vendeur=" + vendeur +
                ", vehicule=" + vehicule +
                '}';
    }
}
