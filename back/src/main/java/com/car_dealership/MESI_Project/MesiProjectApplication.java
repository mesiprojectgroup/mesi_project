package com.car_dealership.MESI_Project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MesiProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MesiProjectApplication.class, args);
	}

}
